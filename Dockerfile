# Use a Rust slim base image
#FROM rust:slim AS builder
# Use an Alpine-based Rust image as the builder stage
FROM rust:alpine AS builder

# Set up the working directory
WORKDIR /usr/src/app

# Install build dependencies
RUN apk add --no-cache build-base musl-dev

# Copy the Cargo.toml and Cargo.lock files
COPY Cargo.toml Cargo.lock ./

# Copy the rest of the application code
COPY src ./src

# Build the application
RUN cargo build --release

# Final stage: Use a minimal base image and copy only necessary files
#FROM scratch

# Final stage: Use an Alpine base image
FROM alpine:3.14

# Final stage: Use a smaller image
#FROM debian:buster-slim

# Set the working directory
WORKDIR /usr/src/app

# Copy the built binary from the builder stage
#COPY --from=builder /usr/src/app/target/debug/proj_4_bin .

# Copy the built binary from the builder stage
COPY --from=builder /usr/src/app/target/release/proj_4_bin .

# Expose the port your application listens on
EXPOSE 8080

# Command to run the application
CMD ["./proj_4_bin"]