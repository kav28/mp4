# mp4

Proj4 README

To do this project, I took the following steps: 
1. **Created the Rust Actix Web Service:**
    - Started by creating a simple Rust Actix web application. Created a new package with Cargo.
    - Added Actix-web as a dependency in my `Cargo.toml` file.
    - Wrote my Actix web service code. This was simply a "Hello, World!" example to start with.
2. **Containerized the Web Service:**
    - Wrote a Dockerfile to define the environment for your application. 
3. **Built a Docker Image:**
    - Used the Dockerfile I created to build a Docker image. 
4. **Ran Container Locally:**
    - After successfully building the Docker image, I ran it locally using the following command: docker run -p 8080:8080 rust-actix-web